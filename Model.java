package bcas.inher.smartdevices;

public class Model extends Os {
String modelname;
int modelnumber;
String releasedate;

public Model(String osname, int osversion, String osreleasedate,String modelname,int modelnumber,String releasedate) {
	super(osname,osversion,osreleasedate);
	this.modelname = modelname;
	this.modelnumber = modelnumber;
	this.releasedate = releasedate;

}
}
