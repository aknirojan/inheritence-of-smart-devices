package bcas.inher.smartdevices;

public class Shape extends Specification {
	String shape;
	String screenmodel;
	
	public Shape(int imeinumber,int ramsize,String networkname,String shape,String screenmodel) {
		super(screenmodel, imeinumber,screenmodel, ramsize,ramsize, networkname);
		this.shape = shape;
		this.screenmodel = screenmodel;
	}
}